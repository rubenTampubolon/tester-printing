'use strict'

function showDatatableFromJson(tableName, jsonData, columns) {

    if ($.fn.DataTable.isDataTable('#' + tableName)) {
        $('#' + tableName).DataTable().destroy();
    }

    var dTables = $("#" + tableName).DataTable({
        responsive: true,
        searching: false,
        autoWidth: true,
        lengthChange: false,
        data: jsonData,
        columnDefs: [
            {
                searchable: false,
                orderable: false,
                targets: 0
            }
        ],
        columns: columns,
        order: [[1, 'asc']],
    });

    dTables.on('order.dt search.dt', function () {
        dTables.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}


function setDatePicker(input) {
    $(input).datetimepicker({
        format: "YYYY-MM-DD",
        useCurrent: false
    })
}

function setDateRangePicker(input1, input2) {
    $(input1).datetimepicker({
        format: "YYYY-MM-DD",
        useCurrent: false
    })
    $(input1).on("change.datetimepicker", function (e) {
        $(input2).val("")
        $(input2).datetimepicker('minDate', e.date);
    });

    $(input2).datetimepicker({
        format: "YYYY-MM-DD",
        useCurrent: false
    })
}

function getValueAfterTax(value) {
    return value + (value * 0.1);
}

function convertToRupiah(number) {
    return new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR"
    }).format(number);
}


function numberToMoney(number) {
    const format = number.toString().split('').reverse().join('');
    const convert = format.match(/\d{1,3}/g);
    return convert.join('.').split('').reverse().join('')
}

function moneyToNumber(money) {
    return parseInt(money.replace(/,.*|[^0-9]/g, ''), 10);
}
